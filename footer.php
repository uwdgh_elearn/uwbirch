<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package uw_wp_theme
 */

?>

		<footer id="colophon" class="site-footer">
			<a href="<?php echo home_url('/'); ?>" class="footer-wordmark"><?php echo get_bloginfo('name'); ?></a>

			<a hidden class="be-boundless hide" href="https://www.washington.edu/boundless/">Be boundless</a>

			<div class="h4" id="social_preface">Connect with us:</div>
			<nav aria-labelledby="social_preface">
				<ul class="footer-social">
					<li hidden class="hide"><a class="facebook" href="https://www.facebook.com/UofWA">Facebook</a></li>
					<li><a class="twitter" href="https://twitter.com/uwbirch">Twitter</a></li>
					<li hidden class="hide"><a class="instagram" href="https://instagram.com/uofwa">Instagram</a></li>
					<li><a class="youtube" href="https://www.youtube.com/channel/UCk_7B2mSlbCZOwM9HVzS1_g">YouTube</a></li>
					<li hidden class="hide"><a class="linkedin" href="https://www.linkedin.com/company/university-of-washington">LinkedIn</a></li>
					<li hidden class="hide"><a class="pinterest" href="https://www.pinterest.com/uofwa/">Pinterest</a></li>
				</ul>
			</nav>

			<nav aria-label="footer">
				<?php uw_wp_theme_footer_menu(); ?>
				<ul id="footer-login" class="footer-links small">
					<li><?php uwbirch_login_link(); ?></li>
				</ul>
			</nav>

			<div class="site-info">
				<p>&copy; <?php echo date( 'Y' ); ?> University of Washington  |  Seattle, WA</p>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page-inner -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
