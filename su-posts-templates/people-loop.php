<?php defined( 'ABSPATH' ) || exit; ?>

<?php
/**
 * READ BEFORE EDITING!
 *
 * Do not edit templates in the plugin folder, since all your changes will be
 * lost after the plugin update. Read the following article to learn how to
 * change this template or create a custom one:
 *
 * https://getshortcodes.com/docs/posts/#built-in-templates
 */
?>
<?php

/**
 * custom ordering by id's listed in the id attribute
 */
if ( ( $atts['orderby'] == 'id_order' ) && ( strlen($atts['id']) > 0 ) ) {
	
	// create an array from the provided id's
	$ids_array = explode(',', $atts['id']);
	
	// store the array of posts objects in a variable
	$queried_posts = $posts->posts;

	/**
	 * anonymous closure function to custom sort by the order of the ids provided in the shortcode
	 * @link: https://stackoverflow.com/questions/11145393/sorting-a-php-array-of-arrays-by-custom-order/11145568#11145568
	 */
	usort($queried_posts, function ($a, $b) use ($ids_array) {
    $pos_a = array_search($a->ID, $ids_array);
    $pos_b = array_search($b->ID, $ids_array);
    return $pos_a - $pos_b;
	});

	// set the posts array (of post objects) with the reordered array
	$posts->posts = $queried_posts;

}

?>
<div class="su-posts su-posts-people-loop <?php echo esc_attr( $atts['class'] ); ?>">

	<div class="row equal">

	<?php if ( $posts->have_posts() ) : ?>

		<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

			<?php
			global $post;
			$people_job_titles = pods_field('persons', get_the_ID(), 'job_title', false);
			?>

			<div id="su-people-<?php the_ID(); ?>" class="col-12 col-sm-6 col-lg-4 col-xl-3 pb-3 <?php echo esc_attr( $atts['class_single'] ); ?>">

				<div class="card card-person text-left image-top white " style="width:240px;">
					<?php if ( has_post_thumbnail( get_the_ID() ) ) : ?>
						<div><a href="<?php the_permalink(); ?>" style="text-decoration:none;" title="<?php the_title(); ?>"><img src="<?php echo get_the_post_thumbnail_url( get_the_ID() ); ?>" class="card-img-top" alt="image of <?php the_title(); ?>"></a></div>
					<?php else: ?>
						<div><a href="<?php the_permalink(); ?>" style="text-decoration:none;" title="<?php the_title(); ?>"><img src="<?php echo esc_url(get_stylesheet_directory_uri().'/img/image-placeholder-square.png'); ?>" class="card-img-top" alt="image of <?php the_title(); ?>"></a></div>
					<?php endif; ?>
					<div class="card-body">
						<h5 class="card-title card-title-slant"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
						<div class="udub-slant-divider"><span></span></div>
						<div class="card-person__job-title">
							<?php
							if ($people_job_titles) {
								foreach ($people_job_titles as $key => $value) {
									echo '<p><small>' . $value . '</small></p>';
								}
							}
							?>
						</div>
					</div>
				</div>

			</div>

		<?php endwhile; ?>

	<?php else : ?>
		<p><samp><?php _e( 'Persons not found', 'shortcodes-ultimate' ); ?></samp></p>
	<?php endif; ?>

	</div>

</div>
