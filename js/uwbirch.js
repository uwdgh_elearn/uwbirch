(function ($, window, document) {

  // The document ready event executes when the HTML-Document is loaded
  // and the DOM is ready.
  jQuery(document).ready(function( $ ) {

  });//document.ready

  // The window load event executes after the document ready event,
  // when the complete page is fully loaded.
  //jQuery(window).load(function () {
  jQuery(window).on('load', function () {

  });//window.load

})(jQuery, this, this.document);
