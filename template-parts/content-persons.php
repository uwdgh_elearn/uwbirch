<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package uw_wp_theme
 */

?>
<?php
$people_job_title = pods_field('persons', get_the_ID(), 'job_title', true);
$people_job_titles = pods_field('persons', get_the_ID(), 'job_title', false);
$people_email = pods_field('persons', get_the_ID(), 'email', true);
$people_link = pods_field('persons', get_the_ID(), 'link', true);
?>
<style>
.people-job-title {
	font-style: italic; }
@media (min-width: 768px) {
	.people-featured-image {
		float: right;	}
	.people-featured-image img {
		margin:0 0 0 1rem;	}
}
</style>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<?php
	//if ( ( is_single() || is_home() ) && get_option( 'show_byline_on_posts' ) ) :
		?>
		<!-- <div class="author-info">
		<?php //if ( function_exists( 'coauthors' ) ) { coauthors(); } else { the_author(); } ?>
		<p class="author-desc"> <small><?php //the_author_meta(); ?></small></p>
		</div> -->

		<?php //endif; ?>

	<div class="entry-content">

 		<div class="people-job-title">
		 <p>
			<?php
			if ($people_job_titles) {
				foreach ($people_job_titles as $key => $value) {
					echo $value . '<br>';
				}
			}
			?>
			</p>
		</div>

	 <?php if ( has_post_thumbnail() ) :  ?>
		 <div class="people-featured-image"><p> <?php the_post_thumbnail( array(300, 300) ,  ['alt' => get_the_title(), 'title' => 'Feature image']);?> </p></div>
	 <?php endif;  ?>

		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'uw_wp_theme' ),
				'after'  => '</div>',
			)
		);
		?>
		<br>
		<div class="people-email"><p>Contact: 
			<?php foreach ($people_email as $_email) { ?>
				<a href="mailto:<?php echo $_email; ?>"><?php echo $_email; ?></a>&nbsp;
			<?php } ?>
		</p></div>
		<div class="people-link"><p><a href="<?php echo esc_url($people_link); ?>"><?php echo $people_link; ?></a></p></div>

	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				uw_wp_theme_edit_post_link();
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
