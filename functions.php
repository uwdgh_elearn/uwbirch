<?php
/**
 * UW WP Theme Child Theme.
 */

 // enqueue child childsheet, require the uw_wp_theme bootstrap style sheet

add_action( 'wp_enqueue_scripts', 'uw_child_enqueue_styles', 11 );

function uw_child_enqueue_styles() {
	$parenthandle = 'uw_wp_theme-bootstrap';

	wp_enqueue_style( 'uw_wp_theme-child-style', get_stylesheet_uri(),
        array( $parenthandle ),
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );

	// local js
	wp_register_script('uwbirch', get_stylesheet_directory_uri() . '/js/uwbirch.js', array('jquery'));
	wp_enqueue_script('uwbirch');
}

/**
* Login/Logout link
*/
function uwbirch_login_link() {
  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    echo '<a href="' . wp_logout_url(get_permalink()) . '" title="' . __('Log out', 'uwbirch') . '">' . __('Log out', 'uwbirch') . '&hellip;&nbsp;<em>' . $current_user->user_login . '</em></a>';
  } else {
    echo '<a href="' . wp_login_url(get_permalink()) . '" title="' . __('Log in', 'uwbirch') . '">' . __('Log in', 'uwbirch') . '</a>';
  }
}

/**
 * implement hook wp_dashboard_setup
 */
add_action( 'wp_dashboard_setup', 'uwbirch_dashboard_setup', 11 );

/**
 * Callback function for hook uwbirch_dashboard_setup
 * Priority 11 (default + 1)
 */
function uwbirch_dashboard_setup() {
	
	// remove dashboard meta boxes for non-admins
	$user = wp_get_current_user();
	if ( ( (!in_array( 'administrator', (array) $user->roles ) || !current_user_can('manage_options')) ) ) {

		// remove core widgets
		global $wp_meta_boxes;
		foreach( $wp_meta_boxes["dashboard"] as $position => $core ) {
			
			foreach( $core["core"] as $widget_id => $widget_info ){
				
				// keep the 'dashboard_activity' core widget
				if ( $widget_id != 'dashboard_activity' ) {

					remove_meta_box( $widget_id, 'dashboard', $position );

				}

			}

		}
		
	}
	
	// add custom dashboard widgets
	// add resources widget
	wp_add_dashboard_widget(
		'dashboard_uwbirch_resources_widget',		//Widget ID (used in the 'id' attribute for the widget).
		'WordPress Resources',		//Title of the widget.
		'dashboard_uwbirch_resources_widget_content',		//Callback function echoing its output.
		null,		//Function that outputs controls for the widget.
		null,		//Data that should be set as the $args property of the widget array (which is the second parameter passed to your callback).
		'side',		//The context within the screen where the box should display.
		'default'		//The priority within the context where the box should show.
	);
	
	/**
	 * implement hook default_hidden_meta_boxes
	 */
	add_filter( 'default_hidden_meta_boxes' , 'uwbirch_hidden_meta_boxes', 11, 2);

}

/**
* Callback function for wp_add_dashboard_widget
*/
function dashboard_uwbirch_resources_widget_content() {

	echo '<style> #dashboard_uwbirch_resources_widget .columns2 { width: 50%; display: inline-block; vertical-align: top;} </style>';

	echo '<div class="columns2">';

	echo '<h3>WordPress Manual</h3>';
	echo '<p><a href="' . esc_url('https://ewp.guide/go/wordpress-manual') . '" rel="nofollow noreferrer" target="_blank">Easy WP Guide WordPress Manual for WordPress</a> <span aria-hidden="true" class="dashicons dashicons-external"></span><p>';
	echo '<ul><li><a href="' . esc_url('https://ewp.guide/go/pages') . '" rel="nofollow noreferrer" target="_blank">—Pages</a> <span aria-hidden="true" class="dashicons dashicons-external"></span></li><li><a href="' . esc_url('https://ewp.guide/go/ce/classic-editor') . '" rel="nofollow noreferrer" target="_blank">—Classic Editor</a> <span aria-hidden="true" class="dashicons dashicons-external"></span></li><li><a href="' . esc_url('https://ewp.guide/go/media-library') . '" rel="nofollow noreferrer" target="_blank">—Media Library</a> <span aria-hidden="true" class="dashicons dashicons-external"></span></li></ul>';

	echo '</div>';

	echo '<div class="columns2">';
	
	echo '<h3>UW Theme Shortcodes</h3>';
	echo '<p><a href="' . esc_url('https://www.washington.edu/docs/shortcode-cookbook/') . '" rel="nofollow noreferrer" target="_blank">UW Shortcode Cookbook</a> <span aria-hidden="true" class="dashicons dashicons-external"></span><p>';
	
		
	echo '<h3>WordPress Training</h3>';
	echo '<p><a href="' . esc_url('https://www.linkedin.com/learning/wordpress-essential-training-22616273') . '" rel="nofollow noreferrer" target="_blank">WordPress Essential Training (LinkedIn Learning)</a> <span aria-hidden="true" class="dashicons dashicons-external"></span> (UW NetID required)<p>';

	echo '</div>';

	echo '<h3 style="text-align: center">WordPress introduction video</h3>';
	echo '<iframe width="100%" height="288" src="https://www.youtube.com/embed/8OBfr46Y0cQ" allowfullscreen></iframe>';

}

/**
 * Callback for hook default_hidden_meta_boxes
 * Filters the default list of hidden meta boxes.
 */
function uwbirch_hidden_meta_boxes( $hidden, $screen ) {

	$user = wp_get_current_user();
	if ( ( (!in_array( 'administrator', (array) $user->roles ) || !current_user_can('manage_options')) ) ) {
		
		// hide the UW Theme dashboard widget for non-admins
		$hidden[] ='uw-dashboard-widget';

	}

	return $hidden;

}